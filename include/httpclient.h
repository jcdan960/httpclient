#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H
#include <thread>
#include <chrono>
#include <json.hpp>
#include <asio.hpp>
#include <asio/ts/buffer.hpp>
#include <asio/ts/internet.hpp>
#define CPPHTTPLIB_OPENSSL_SUPPORT
#include <httplib.h>
#include <include/CurrencyAPI.h>

#ifndef ASIO_STANDALONE
#define ASIO_STANDALONE
#endif // !ASIO_STANDALONE

class HTTPClient{ 
public:
	HTTPClient(json::JSON & cfg, std::shared_ptr<CurrencyAPI> ptr);
	~HTTPClient();
	void Run();
	void Stop();
	static bool ResolveDNS(std::string const &url, std::string const &port, asio::ip::tcp::endpoint &endPoint);

private:
	bool _killThread;
	bool _secure;
	std::string _port;
	std::thread _comThread;
	std::chrono::milliseconds _sleep;
	std::string _RESTAPIserver;
	std::string _pathToLatest;
	std::shared_ptr<CurrencyAPI> _currencyAPI;
	std::thread _requestsThread;

	void _launchRequestsThread();
	void _runHttp();
	void _runHttps();
};
#endif // !HTTPCLIENT_H
