#include <include/httpclient.h>
#include <include/SysLog.h>

HTTPClient::HTTPClient(json::JSON & cfg, std::shared_ptr<CurrencyAPI> ptr): 
	_killThread(false),
	_currencyAPI(ptr) 
{
	auto sleep = cfg.at("SleepBetweenAPICallsMs").ToInt();
	_sleep = std::chrono::milliseconds(sleep);

	_secure = cfg.at("Secure").ToBool();
	_port = _secure ? "443" : "80";

	_RESTAPIserver = cfg.at("RestApiServer").ToString();
	_pathToLatest = cfg.at("LatestDataPath").ToString();
}

HTTPClient::~HTTPClient() {
	Stop();
}

void HTTPClient::_launchRequestsThread() {
	if (!_requestsThread.joinable()) {
		if (_secure)
			_requestsThread = std::thread(&HTTPClient::_runHttps, this);
		else
			_requestsThread = std::thread(&HTTPClient::_runHttp, this);
	}
}

void HTTPClient::Run() {

	_launchRequestsThread();
}

void HTTPClient::Stop() {
	if (_comThread.joinable())
		_comThread.join();

	_killThread = true;
}

void HTTPClient::_runHttp() {

	asio::ip::tcp::endpoint endPoint;
	if (!ResolveDNS(_RESTAPIserver, _port, endPoint))
		return;

	asio::error_code ec;

	asio::io_context context;

	asio::ip::tcp::socket socket(context);

	socket.connect(endPoint, ec);

	if (!ec)
		if (socket.is_open())
			SysLog::Log(SysLog::SeverityLevel::Notice, "HTTP client connected to server " + _RESTAPIserver);

		else {
			SysLog::Log(SysLog::SeverityLevel::Alert, "HTTP client could not connect to server " + _RESTAPIserver);
			SysLog::Log(ec.message());
		}

}

void HTTPClient::_runHttps() {

	std::unique_ptr<httplib::Client> client = std::make_unique<httplib::Client>(_RESTAPIserver.c_str());

	_killThread = false;

	while (!_killThread) {

		for (auto its = _currencyAPI->Begin(); its != _currencyAPI->End(); ++its){

#ifndef NDEBUG 
			auto startSingleCurrencies = std::chrono::high_resolution_clock::now();
#endif // !NDEBUG 
			auto response = client->Get((_pathToLatest + its->first).c_str());

#ifndef NDEBUG 
			auto endSingleCurrencies = std::chrono::high_resolution_clock::now();
			auto us = std::chrono::duration_cast<std::chrono::microseconds>(endSingleCurrencies - startSingleCurrencies);
			SysLog::Log(SysLog::SeverityLevel::Debug, "Single currency HTTP Request round trip time (RTT): " + std::to_string(us.count()) + " microseconds");
#endif // !NDEBUG 

			if (response->status != 200) {
#ifndef NDEBUG
				SysLog::Log(SysLog::SeverityLevel::Alert,"Error running https client, received status code " + std::to_string(response->status));
#endif // NDEBUG
				continue;
			}

#ifndef NDEBUG
			SysLog::Log(SysLog::SeverityLevel::Debug, "New currencies data received from http server", true);
#endif // !NDEBUG

			_currencyAPI->UpdateValues(its->first, response->body);

		}

		_currencyAPI->SetNewDataAvailable(true);
		std::this_thread::sleep_for(_sleep);
	}

}

bool HTTPClient::ResolveDNS(std::string const& url, std::string const &port ,asio::ip::tcp::endpoint &endPoint) {
	std::string returnStr;
	SysLog::Log(SysLog::SeverityLevel::Notice, "Resolving DNS for URL " + url);

	asio::io_service ios;
	asio::ip::tcp::resolver::query resolverQuery(url, port, asio::ip::tcp::resolver::query::numeric_service);

	asio::ip::tcp::resolver resolver(ios);

	asio::error_code ec;

	asio::ip::tcp::resolver::iterator it = resolver.resolve(resolverQuery, ec);

	if (ec) {
		SysLog::Log(SysLog::SeverityLevel::Error, "Error resolving DNS: " + ec.message());
		return false;
	}

	else {
		SysLog::Log(SysLog::SeverityLevel::Notice, "DNS resolved");
		endPoint = it->endpoint();
		return true;
	}

}

//https://exchangeratesapi.io/
//https://www.youtube.com/watch?v=2hNdkYInj4g
